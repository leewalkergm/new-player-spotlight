package com.gmail.leewalkergm.daylight;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class LoginListener implements Listener {
	protected NewPlayerSpotlight _plugin;
	
	//Self registering listener!
	public LoginListener(NewPlayerSpotlight plugin) {
		_plugin = plugin;
		_plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player plr = event.getPlayer();
		
		// If anything is null, log error and return
		if(event == null || plr == null) {
			_plugin.getLogger().severe("Player or Event was NULL.");
			return;
		}
		
		//Get the world that the player belongs to
		World world = event.getPlayer().getWorld();
		
		//If the player's world is null (or the player doesn't belong to a world) log the error and return
		if(world == null) {
			_plugin.getLogger().severe("Player `" + plr.getName() + "` does not appear to belong to a world.");
			return;
		}
		
		
		Boolean returningPlayer = plr.hasPlayedBefore();
		
		Boolean greet = _plugin.getConfig().getBoolean("greet.enabled");
		Boolean greetAllPlayers = !_plugin.getConfig().getBoolean("greet.newplayersonly");
		
		Boolean setDaylight = _plugin.getConfig().getBoolean("daylight.enabled");
		Boolean setDaylightBroadcast = _plugin.getConfig().getBoolean("daylight.broadcast");
		
		//All is good, do the plugin stuff D:
		
		//Send a greeting to the player
		if( greet && (!returningPlayer || greetAllPlayers)) {
			String greetMessage = _plugin.getConfig().getString("greet.message").replaceAll("%plr", plr.getName());
			plr.sendMessage(greetMessage);
		}
		
		//
		if(setDaylight && !returningPlayer) {
			
			// Set the time of the world and send a message
			world.setFullTime(600);
			if(setDaylightBroadcast) {
				String daylightMessage = _plugin.getConfig().getString("daylight.message").replaceAll("%plr", plr.getName());
				_plugin.getServer().broadcastMessage(daylightMessage);
			}
		}
	}
}
